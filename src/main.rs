#[macro_use]
extern crate gfx;

extern crate gfx_window_glutin;
extern crate glutin;
extern crate image;
extern crate cgmath;

use gfx::traits::FactoryExt;
use gfx::Device;
use gfx_window_glutin as gfx_glutin;
use glutin::{GlContext, GlRequest};
use glutin::Api::OpenGl;

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;

const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];

gfx_defines!{
    vertex Vertex {
        pos: [f32; 2] = "a_Pos",
        uv: [f32; 2] = "a_Uv",
    }

    constant Transform {
        transform: [[f32; 4];4] = "u_Transform",
    }

    pipeline my_pipeline {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        transform: gfx::ConstantBuffer<Transform> = "Transform",
        tex: gfx::TextureSampler<[f32; 4]> = "t_Texture",
        out: gfx::BlendTarget<ColorFormat> = ("Target0", gfx::state::MASK_ALL, gfx::preset::blend::ALPHA),
    }
}

fn gfx_load_texture<F, R>(factory: &mut F) -> gfx::handle::ShaderResourceView<R, [f32; 4]>
    where F: gfx::Factory<R>,
          R: gfx::Resources
{
    use gfx::format::Rgba8;

    let img = image::open("resources/image.png").unwrap().to_rgba();
    let (w, h) = img.dimensions();
    let kind = gfx::texture::Kind::D2(w as u16, h as u16, gfx::texture::AaMode::Single);
    let (_, view) = factory.create_texture_immutable_u8::<Rgba8>(kind, &[&img]).unwrap();
    view
}

pub fn main() {
    use Transform;

    const GFX_WIDTH: u32 = 512;
    const GFX_HEIGHT: u32 = 512;

    let mut events_loop = glutin::EventsLoop::new();
    let window_builder = glutin::WindowBuilder::new()
        .with_title("Texture Example".to_string())
        .with_dimensions(GFX_WIDTH, GFX_HEIGHT);
    let context_builder = glutin::ContextBuilder::new()
        .with_gl(GlRequest::Specific(OpenGl,(3,2)))
        .with_vsync(true);
    let (window, mut device, mut factory, color_view, _depth_view) =
        gfx_glutin::init::<ColorFormat, DepthFormat>(window_builder, context_builder, &events_loop);

    let pso = factory.create_pipeline_simple(
        include_bytes!("shaders/vertex.glsl"),
        include_bytes!("shaders/pixel.glsl"),
        my_pipeline::new()
    ).unwrap();

    let mut encoder: gfx::Encoder<_, _> = factory.create_command_buffer().into();

    const SIDE_SIZE: f32 = 100.0;
    const LEFT: f32 = 20.0;
    const TOP: f32 = 10.0;
    const RIGHT: f32 = LEFT + SIDE_SIZE;
    const BOTTOM: f32 = TOP + SIDE_SIZE;

    const QUAD: [Vertex; 6] = [
        Vertex { pos: [ LEFT,  TOP    ], uv: [0.0, 0.0] },
        Vertex { pos: [ RIGHT, TOP    ], uv: [1.0, 0.0] },
        Vertex { pos: [ RIGHT, BOTTOM ], uv: [1.0, 1.0] },

        Vertex { pos: [ LEFT,  BOTTOM ], uv: [0.0, 1.0] },
        Vertex { pos: [ LEFT,  TOP    ], uv: [0.0, 0.0] },
        Vertex { pos: [ RIGHT, BOTTOM ], uv: [1.0, 1.0] },
    ];

    let transform = {
        let mtx: cgmath::Matrix4<f32> = cgmath::ortho(0.0, GFX_WIDTH as f32,
                                                      GFX_HEIGHT as f32, 0.0,
                                                      -1.0, 1.0);
        Transform { transform: mtx.into() }
    };

    let (vertex_buffer, slice) = factory.create_vertex_buffer_with_slice(&QUAD, ());
    let sampler = factory.create_sampler_linear();
    let texture = gfx_load_texture(&mut factory);

    let data = my_pipeline::Data {
        vbuf: vertex_buffer,
        transform: factory.create_constant_buffer(1),
        tex: (texture, sampler),
        out: color_view.clone(),
    };

    let mut running = true;
    while running {
        events_loop.poll_events(|event| {
            if let glutin::Event::WindowEvent { event, .. } = event {
                match event {
                    glutin::WindowEvent::Closed |
                    glutin::WindowEvent::KeyboardInput {
                        input: glutin::KeyboardInput {
                            virtual_keycode: Some(glutin::VirtualKeyCode::Escape), ..
                        }, ..
                    } => running = false,
                    _ => {}
                }
            }
        });

        encoder.clear(&color_view, BLACK); //clear the framebuffer with a color(color needs to be an array of 4 f32s, RGBa)
        encoder.update_buffer(&data.transform, &[transform], 0).unwrap();
        encoder.draw(&slice, &pso, &data); // draw commands with buffer data and attached pso
        encoder.flush(&mut device); // execute draw commands

        window.swap_buffers().unwrap();
        device.cleanup();
    }
}
