#version 150 core

uniform Transform {
    mat4 u_Transform;
};

in vec2 a_Pos;
in vec2 a_Uv;

out vec2 v_Uv;

void main() {
    v_Uv = a_Uv;
    gl_Position = u_Transform * vec4(a_Pos, 0, 1);
}
